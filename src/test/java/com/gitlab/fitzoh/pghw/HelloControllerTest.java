package com.gitlab.fitzoh.pghw;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest
public class HelloControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void getWithNoAcceptReturnsHTML() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(content().string("<p>Hello, World</p>"));
    }

    @Test
    public void getAcceptingJsonReturnsJson() throws Exception {
        mockMvc.perform(get("/").header("Accept", "application/json"))
                .andExpect(content().json("{\"message\": \"Good morning\"}"));
    }


    //Homework didn't define post behavior, so I'm assuming it takes a name parameter and returns a message using that name
    @Test
    public void postWithNameReturnsMessageWithName() throws Exception {
        mockMvc.perform(post("/").content("{\"name\": \"PlanGrid\"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"message\": \"Good morning PlanGrid\"}"));
    }
}
