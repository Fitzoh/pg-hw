package com.gitlab.fitzoh.pghw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.Instant;

@Component
public class PgLoggingFilter implements Filter {


    private static final Logger log = LoggerFactory.getLogger(PgLoggingFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //noop
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        log.debug("{}: {}", Instant.now(), httpRequest.getRequestURL());
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        //noop
    }
}
