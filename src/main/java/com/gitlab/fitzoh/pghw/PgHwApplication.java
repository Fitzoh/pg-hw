package com.gitlab.fitzoh.pghw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PgHwApplication {

	public static void main(String[] args) {
		SpringApplication.run(PgHwApplication.class, args);
	}
}
