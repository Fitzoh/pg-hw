package com.gitlab.fitzoh.pghw;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping(path = "/")
    public ResponseEntity<String> helloHTML() {
        return ResponseEntity.ok("<p>Hello, World</p>");
    }

    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HelloMessage> helloJson() {
        return ResponseEntity.ok(new HelloMessage("Good morning"));
    }

    //Homework didn't define post behavior, so I'm assuming it takes a name parameter and returns a message using that name
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HelloMessage> helloName(@RequestBody HelloRequest HelloRequest) {
        String message = String.format("Good morning %s", HelloRequest.getName());
        return ResponseEntity.ok(new HelloMessage(message));
    }
}
