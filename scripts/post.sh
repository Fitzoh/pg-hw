#!/usr/bin/env bash

set -x

curl -X POST -H "Content-Type: Application/json" \
             -H "Accept: Application/json" \
             -d '{"name": "PlanGrid"}' localhost:8080/
