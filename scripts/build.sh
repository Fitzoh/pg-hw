#!/usr/bin/env bash

docker build . --target builder -t pg-hw:builder

docker build . --target app -t pg-hw:app