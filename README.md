### Assumptions
The homework did not clearly define the behavior for POST requests, so I assumed that it accepted a JSON message with a name, and returned a greeting containing that name.

### Prerequisites
Docker: Installation instructions can be found [here](https://docs.docker.com/install/)

Tested locally against `Docker version 18.06.0-ce, build 0ffa825`

### Building
From the root of the repository, run `scripts/build.sh`

This creates two docker images, `pg-hw:builder` and `pg-hw:app`.

The `builder` image is a temporary workspace where the project is compiled and tests are run.

The `app` image contains only the final jar.


### Testing
Building the project runs the tests. If you wish to run the tests again, you can re-run them using the `builder` image:

`docker run pg-hw:builder ./gradlew --rerun-tasks test`


### Running
The app exposes port 8080 in the docker container.

All following steps assume you are running the app with this docker command:

`docker run --rm -d --name pg-hw -p 8080:8080 pg-hw:app`



### Test scripts
There are scripts to test each of the endpoints individually in the `scripts` directory
```
$ sh scripts/get-html.sh
+ curl localhost:8080/
<p>Hello, World</p>

$ sh scripts/get-json.sh
+ curl -H 'Accept: application/json' localhost:8080/
{"message":"Good morning"}

$ sh scripts/post.sh
+ curl -X POST -H 'Content-Type: Application/json' -H 'Accept: Application/json' -d '{"name": "PlanGrid"}' localhost:8080/
{"message":"Good morning PlanGrid"}
```

### Logging
If you wish to activate the timestamp/url logging, you can run the app with an additional environment variable set
`docker run --rm -d --name pg-hw -p 8080:8080 -e LOGGING_LEVEL_COM_GITLAB_FITZOH=debug pg-hw:app`
To view the logs, run `docker logs pg-hw` (or `docker logs pg-hw -f` to tail them)

### Killing the app
`docker kill pg-hw`
