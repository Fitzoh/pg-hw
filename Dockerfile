FROM openjdk:8 AS builder
ADD . .
RUN ./gradlew build

FROM openjdk:8 as app
COPY --from=builder build/libs/*.jar pg-hw.jar
EXPOSE 8080
CMD ["java", "-jar", "pg-hw.jar"]